let postTemplate = $('#post-template').html();
let renderPost = Handlebars.compile(postTemplate);

let postsTemplate = $('#posts-template').html();
let renderPosts = Handlebars.compile(postsTemplate);

let posts = [];

$('#post-button').on('click', function() {
  let postBody = $('#wall-post').val();
  $.ajax({
    type: 'POST',
    url: 'http://localhost:3000/api/wall-posts',
    data: {
      body: postBody,
      createdAt: new Date()
    }
  }).then(function(response) {
    // Approach 1
    // $('#wall-posts').append(`<div>${postBody}</div>`);

    // Approach 2
    // $('#wall-posts').append(renderPost(response));

    // Approach 3
    posts.push(response);
    $('#wall-posts').html(renderPosts({
      posts: posts
    }));
  });
});

$.ajax({
  type: 'GET',
  url: 'http://localhost:3000/api/wall-posts'
}).then(function(response) {
  // Approach 1
  // response.forEach(function(post) {
  //   $('#wall-posts').append(renderPost(post));
  // });

  // Approach 2
  // let content = '';
  // response.forEach(function(post) {
  //   content += renderPost(post);
  // });
  // $('#wall-posts').append(content);

  // Approach 3
  $('#wall-posts').append(renderPosts({
    posts: response
  }));

  // part of Approach 3 for creating a post
  posts = response;
});
